/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.plunts.gradle.plantuml.plugin;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ScanResult;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.AbstractClasspathMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.output.AbstractDiagramWriter;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.inject.Inject;
import lombok.Getter;
import org.gradle.api.DefaultTask;
import org.gradle.api.InvalidUserDataException;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.file.FileCollection;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.OutputFiles;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.compile.JavaCompile;

import static java.util.stream.Collectors.toSet;

/**
 * Gradle task that generates class PlantUML diagrams.
 */
public class GenerateClassDiagramsTask extends DefaultTask {

  @Input
  @Getter
  private final List<ClassDiagramsExtension.ClassDiagram> diagrams;
  @Input
  @Getter
  private final String plantumlServer;
  private final Set<File> projectClasspath = new HashSet<>();
  private FileCollection dependencyClasspath = null;
  @InputFiles
  @Getter
  private final Set<File> allSources = new HashSet<>();
  @OutputFiles
  @Getter
  public final Set<File> outputFiles;

  @Inject
  public GenerateClassDiagramsTask(final ClassDiagramsExtension extension) {
    this.diagrams = extension.getDiagrams();
    this.plantumlServer = extension.getPlantumlServer();
    this.outputFiles = extension.getDiagrams().stream()
            .map(ClassDiagramsExtension.ClassDiagram::getWriters)
            .flatMap(List::stream)
            .map(AbstractDiagramWriter::getFile)
            .map(f -> {
              try {
                return f.getCanonicalFile();
              } catch (IOException ex) {
                return f.getAbsoluteFile();
              }
            })
            .collect(toSet());
    setGroup("Documentation");
    setDescription("Generates PlantUML diagrams for the main source code.");
  }

  void addProjectClasspath(final Project project) {
    final SourceSetContainer sourceSetContainer = project.getExtensions().findByType(SourceSetContainer.class);
    if (sourceSetContainer != null && !sourceSetContainer.isEmpty()) {
      SourceSet sourceSet = sourceSetContainer.getByName("main");
      this.projectClasspath.addAll(sourceSet.getOutput().getClassesDirs().getFiles());
      if (dependencyClasspath == null) {
        this.dependencyClasspath = sourceSet.getCompileClasspath();
      } else {
        this.dependencyClasspath = this.dependencyClasspath.plus(sourceSet.getCompileClasspath());
      }
      this.allSources.addAll(sourceSet.getAllSource().getFiles());
      dependsOn(project.getTasksByName(sourceSet.getCompileJavaTaskName(), false));
    }

    try {
      getClass().getClassLoader().loadClass("com.android.build.api.dsl.ApplicationExtension");
      addAndroidClasspath(project);
    } catch (ClassNotFoundException ignore) {
      // android plugin not loaded => everything is fine
    }
  }

  private void addAndroidClasspath(Project project) {
    for (Task task : project.getTasks()) {
      final String taskName = task.getName().toLowerCase(Locale.ROOT);
      if (taskName.contains("compile") && taskName.contains("debug") && !taskName.contains("test")) {
        if (task instanceof JavaCompile) {
          JavaCompile javaCompile = (JavaCompile) task;
          this.projectClasspath.add(javaCompile.getDestinationDirectory().getAsFile().get());
          if (dependencyClasspath == null) {
            this.dependencyClasspath = javaCompile.getClasspath();
          } else {
            this.dependencyClasspath = this.dependencyClasspath.plus(javaCompile.getClasspath());
          }
          this.allSources.addAll(javaCompile.getSource().getFiles());
          dependsOn(task);
        } else if (taskName.contains("kotlin")) {
          this.allSources.addAll(task.getInputs().getSourceFiles().getFiles());
        }
      }
    }
  }

  @TaskAction
  public void generateClassDiagrams() throws IOException {
    for (ClassDiagramsExtension.ClassDiagram diagram : diagrams) {
      generateDiagram(diagram);
    }
  }

  private void generateDiagram(ClassDiagramsExtension.ClassDiagram diagram) throws IOException {
    final ClassGraph classGraph = new ClassGraph()
            .enableClassInfo().ignoreClassVisibility()
            .enableFieldInfo().ignoreFieldVisibility()
            .enableMethodInfo().ignoreMethodVisibility()
            .enableAnnotationInfo()
            .enableExternalClasses()
            .enableInterClassDependencies()
            .overrideClasspath(this.projectClasspath)
            .overrideClasspath(this.dependencyClasspath.getFiles());

    if (diagram.getBaselineIncludeExclude().isEmpty()) {
      throw new InvalidUserDataException("diagram " + diagram.getName() + " must define at least one class or package include");
    }

    Set<String> projectPackages = findProjectPackages();
    if (!projectPackages.isEmpty()) {
      classGraph.acceptPackages(projectPackages.toArray(new String[projectPackages.size()]));
    }

    diagram.getBaselineIncludeExclude().getIncludes().stream().forEach(matcher -> matcher.configureAccept(classGraph));
    diagram.getBaselineIncludeExclude().getExcludes().stream().forEach(matcher -> matcher.configureReject(classGraph));
    diagram.getReferencesIncludeExclude().getIncludes().stream().forEach(matcher -> matcher.configureAccept(classGraph));
    diagram.getSubclassIncludeExclude().getIncludes().stream().forEach(matcher -> matcher.configureAccept(classGraph));

    final ClassDiagramBuilder builder = new ClassDiagramBuilder(diagram);
    AbstractClasspathMatcher.setProjectClasspath(projectClasspath);
    try (ScanResult scanResult = classGraph.scan()) {
      int baselineClasses = 0;
      for (ClassInfo classInfo : scanResult.getAllClasses()) {
        if (diagram.getBaselineIncludeExclude().isIncluded(classInfo)) {
          baselineClasses++;
          builder.addClass(classInfo);
        }
      }
      if (baselineClasses == 0) {
        throw new InvalidUserDataException("diagram " + diagram.getName() + " does not include any classes");
      }

      final String plantuml = builder.build();
      for (AbstractDiagramWriter writer : diagram.getWriters()) {
        writer.setTemporaryDir(getTemporaryDir());
        writer.setPlantumlServer(this.plantumlServer);
        writer.write(diagram.getName(), plantuml);
      }
    } finally {
      AbstractClasspathMatcher.setProjectClasspath(null);
    }
  }

  private Set<String> findProjectPackages() {
    Set<String> result = new HashSet<>();
    for (File file : projectClasspath) {
      if (file.isDirectory()) {
        findProjectPackages(null, file, result);
      }
    }
    return result;
  }

  private boolean findProjectPackages(String prefix, File folder, Set<String> target) {
    boolean hasClasses = false;
    for (File file : folder.listFiles()) {
      if (file.getName().endsWith(".class")) {
        hasClasses = true;
      } else if (file.isDirectory()) {
        String packageName = prefix == null ? file.getName() : prefix + "." + file.getName();
        if (findProjectPackages(packageName, file, target)) {
          target.add(packageName);
        }
      }
    }
    return hasClasses;
  }

}
