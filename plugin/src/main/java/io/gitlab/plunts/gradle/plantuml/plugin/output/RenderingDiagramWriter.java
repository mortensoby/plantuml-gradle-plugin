/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.plunts.gradle.plantuml.plugin.output;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.zip.Deflater;
import org.gradle.api.GradleException;

/**
 * This implementation of {@link AbstractDiagramWriter} uses a PlantUML server to directly render
 * the diagram and write the result to a file. If it does not exist, it will be created (including
 * directories). Any previous content will be overwritten.
 */
public class RenderingDiagramWriter extends AbstractDiagramWriter {

  private final String format;

  /**
   * Constructs the writer with the target file. The file must have an extension indicating the
   * rendering type (e.g. png or svg).
   *
   * @param file the output file
   */
  public RenderingDiagramWriter(File file) {
    super(file);

    final String filename = file.getName();
    final int dot = filename.lastIndexOf('.');
    if (dot < 0) {
      throw new GradleException("File needs an extension: " + filename);
    }
    this.format = filename.substring(dot + 1);
  }

  @Override
  public void write(String name, String diagram) throws IOException {
    final Deflater d = new Deflater(Deflater.BEST_COMPRESSION, true);
    d.setInput(diagram.getBytes(StandardCharsets.UTF_8));
    d.finish();

    byte[] output = new byte[diagram.length() * 2];
    int size = d.deflate(output);

    URI uri = URI.create(getPlantumlServer() + "/" + format + "/" + encode64(output, size));
    getFile().getParentFile().mkdirs();
    Files.copy(uri.toURL().openStream(), getFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
  }

  private String encode64(byte[] output, int size) {
    assert output.length + 2 >= size;

    StringBuilder sb = new StringBuilder(size * 2);
    for (int i = 0; i < size; i += 3) {
      append3bytes(sb, output[i] & 0xFF, output[i + 1] & 0xFF, output[i + 2] & 0xFF);
    }
    return sb.toString();
  }

  private void append3bytes(StringBuilder sb, int b1, int b2, int b3) {
    int c1 = b1 >> 2;
    int c2 = ((b1 & 0x3) << 4) | (b2 >> 4);
    int c3 = ((b2 & 0xF) << 2) | (b3 >> 6);
    int c4 = b3 & 0x3F;
    sb.append(encode6bit(c1 & 0x3F));
    sb.append(encode6bit(c2 & 0x3F));
    sb.append(encode6bit(c3 & 0x3F));
    sb.append(encode6bit(c4 & 0x3F));
  }

  private char encode6bit(int b) {
    if (b < 10) {
      return (char) ('0' + b);
    }
    b -= 10;
    if (b < 26) {
      return (char) ('A' + b);
    }
    b -= 26;
    if (b < 26) {
      return (char) ('a' + b);
    }
    b -= 26;
    if (b == 0) {
      return '-';
    }
    if (b == 1) {
      return '_';
    }
    throw new IllegalArgumentException("Unmappable byte: " + Integer.toHexString(b));
  }

}
