/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.plunts.gradle.plantuml.plugin;

import io.github.classgraph.AnnotationInfo;
import io.github.classgraph.AnnotationInfoList;
import io.github.classgraph.AnnotationParameterValueList;
import io.github.classgraph.ArrayTypeSignature;
import io.github.classgraph.BaseTypeSignature;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ClassRefTypeSignature;
import io.github.classgraph.FieldInfo;
import io.github.classgraph.MethodInfo;
import io.github.classgraph.MethodParameterInfo;
import io.github.classgraph.ReferenceTypeSignature;
import io.github.classgraph.TypeArgument;
import io.github.classgraph.TypeParameter;
import io.github.classgraph.TypeSignature;
import io.github.classgraph.TypeVariableSignature;
import io.gitlab.plunts.gradle.plantuml.plugin.relation.AbstractClassRelation;
import io.gitlab.plunts.gradle.plantuml.plugin.relation.AssociativeRelation;
import io.gitlab.plunts.gradle.plantuml.plugin.relation.ExtensionRelation;
import io.gitlab.plunts.gradle.plantuml.plugin.relation.RelationOverride;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;

/**
 * Parses ClassInfo objects and builds a PlantUML diagram out of them.
 */
class ClassDiagramBuilder {

  private static final String DOTTED_SEPERATOR = "..\n";
  private static final String LINE_SEPERATOR = "__\n";
  private static final String INDENTATION = "  ";
  private static final String PACKAGE_DELIMITER = ".";
  private final ClassDiagramsExtension.ClassDiagram diagram;
  private final PackageContainer basePackage = new PackageContainer();
  private final Set<ClassInfo> allClasses = new HashSet<>();
  private final Map<ClassInfo, Map<ClassInfo, AssociativeRelation>> associativeRelations = new HashMap<>();
  private boolean seperatorBeforeFields;
  private boolean seperatorBeforeMethods;

  ClassDiagramBuilder(ClassDiagramsExtension.ClassDiagram diagram) {
    this.diagram = diagram;
  }

  /**
   * Adds a class to the diagram.
   *
   * @param classInfo the class info
   */
  public void addClass(ClassInfo classInfo) {
    if (classInfo.isSynthetic() || diagram.getBaselineIncludeExclude().isExcluded(classInfo)) {
      return;
    }

    final PackageContainer targetPackage = findPackageContainer(classInfo);
    int idx = Collections.binarySearch(targetPackage.classes, classInfo);
    if (idx >= 0) {
      return;
    }
    targetPackage.classes.add(Math.abs(idx + 1), classInfo);
    allClasses.add(classInfo);

    addHierarchy(classInfo);
    final boolean includeReferences = diagram.getReferencesIncludeExclude().isIncluded(classInfo);
    for (FieldInfo fieldInfo : classInfo.getDeclaredFieldInfo()) {
      if (!fieldInfo.isSynthetic() && !fieldInfo.isEnum()) {
        processFieldReference(classInfo, fieldInfo.getTypeSignatureOrTypeDescriptor(), fieldInfo.getAnnotationInfo(), includeReferences);
      }
    }
  }

  private void addHierarchy(ClassInfo classInfo) {
    if (classInfo.getSuperclass() != null && diagram.getSuperclassIncludeExclude().isIncluded(classInfo)) {
      addClass(classInfo.getSuperclass());
    }
    if (classInfo.getInterfaces() != null && diagram.getInterfaceIncludeExclude().isIncluded(classInfo)) {
      for (ClassInfo interfaceInfo : classInfo.getInterfaces()) {
        addClass(interfaceInfo);
      }
    }
    if (classInfo.getSubclasses() != null && diagram.getSubclassIncludeExclude().isIncluded(classInfo)) {
      for (ClassInfo subclassInfo : classInfo.getSubclasses()) {
        addClass(subclassInfo);
      }
    }
  }

  private PackageContainer findPackageContainer(ClassInfo classInfo) {
    PackageContainer targetPackage = basePackage;
    if (diagram.isShowPackages()) {
      for (String packagePart : classInfo.getPackageName().split("\\" + PACKAGE_DELIMITER)) {
        targetPackage = targetPackage.subPackages.computeIfAbsent(packagePart, t -> new PackageContainer());
      }
    }
    return targetPackage;
  }

  private void processFieldReference(ClassInfo classInfo, TypeSignature typeSignature, AnnotationInfoList annotations, boolean includeClass) {
    if (typeSignature instanceof ClassRefTypeSignature) {
      final ClassRefTypeSignature classRefTypeSignature = (ClassRefTypeSignature) typeSignature;
      if (includeClass && classRefTypeSignature.getTypeArguments().isEmpty()
              && !classRefTypeSignature.getClassInfo().getPackageName().startsWith("java")) {
        addClass(classRefTypeSignature.getClassInfo());
      }

      // first check if opposite is already known
      AssociativeRelation relation = associativeRelations
              .computeIfAbsent(classRefTypeSignature.getClassInfo(), k -> new HashMap<>())
              .get(classInfo);
      AssociativeRelation.Viewpoint left;
      AssociativeRelation.Viewpoint right;
      if (relation == null) {
        // if not, check existing or create it finally
        relation = associativeRelations
                .computeIfAbsent(classInfo, k -> new HashMap<>())
                .computeIfAbsent(classRefTypeSignature.getClassInfo(), k -> new AssociativeRelation());
        left = AssociativeRelation.Viewpoint.SOURCE;
        right = AssociativeRelation.Viewpoint.TARGET;
      } else {
        relation.activateBackReference();
        left = AssociativeRelation.Viewpoint.TARGET;
        right = AssociativeRelation.Viewpoint.SOURCE;
      }

      processJpaAnnotations(annotations, relation, left, right);
      processValidationAnnotations(annotations, relation, right);
      processTypeArguments(classRefTypeSignature, classInfo, annotations, includeClass);
    } else if (typeSignature instanceof ArrayTypeSignature) {
      final ArrayTypeSignature arrayTypeSignature = (ArrayTypeSignature) typeSignature;
      processFieldReference(classInfo, arrayTypeSignature.getElementTypeSignature(), annotations, includeClass);
    }
    // ignore BaseTypeSignature and TypeVariableSignature
  }

  private void processJpaAnnotations(AnnotationInfoList annotations, AssociativeRelation relation, AssociativeRelation.Viewpoint left, AssociativeRelation.Viewpoint right) {
    if (annotations.containsName("javax.persistence.OneToMany")) {
      relation.setArrow(left, AssociativeRelation.AGGREGATION_ARROW);
      relation.setMultiplicityMax(left, 1);
      relation.setMultiplicityMin(right, 0);
    } else if (annotations.containsName("javax.persistence.ManyToOne")) {
      relation.setTargetArrow(AssociativeRelation.AGGREGATION_ARROW);
      relation.setMultiplicityMin(left, 0);
      relation.setMultiplicityMax(right, 1);
    } else if (annotations.containsName("javax.persistence.OneToOne")) {
      relation.setMultiplicityMin(left, 0);
      relation.setMultiplicityMax(left, 1);
      relation.setMultiplicityMin(right, 0);
      relation.setMultiplicityMax(right, 1);
    } else if (annotations.containsName("javax.persistence.ManyToMany")) {
      relation.setMultiplicityMin(left, 0);
      relation.setMultiplicityMin(right, 0);
    } else if (annotations.containsName("javax.persistence.ElementCollection")) {
      relation.setSourceArrow(AssociativeRelation.COMPOSITION_ARROW);
      relation.setMultiplicityMin(left, 1);
      relation.setMultiplicityMax(left, 1);
      relation.setMultiplicityMin(right, 0);
    }
  }

  private void processValidationAnnotations(AnnotationInfoList annotations, AssociativeRelation relation, AssociativeRelation.Viewpoint right) {
    final AnnotationInfo sizeConstraint = annotations.get("jakarta.validation.constraints.Size");
    if (sizeConstraint != null) {
      final AnnotationParameterValueList parameterValues = sizeConstraint.getParameterValues();
      final Integer min = (Integer) parameterValues.getValue("min");
      if (min != null) {
        relation.setMultiplicityMin(right, min);
      }
      final Integer max = (Integer) parameterValues.getValue("max");
      if (max != null) {
        relation.setMultiplicityMax(right, max);
      }
    }
    if (annotations.containsName("jakarta.validation.constraints.NotNull")
            || annotations.containsName("jakarta.validation.constraints.NotEmpty")) {
      relation.setMultiplicityMin(right, 1);
    }
  }

  private void processTypeArguments(final ClassRefTypeSignature classRefTypeSignature, ClassInfo classInfo, AnnotationInfoList annotations, boolean includeClass) {
    for (TypeArgument typeArgument : classRefTypeSignature.getTypeArguments()) {
      final ReferenceTypeSignature referenceTypeSignature = typeArgument.getTypeSignature();
      if (referenceTypeSignature != null) {
        processFieldReference(classInfo, referenceTypeSignature, annotations, includeClass);
      }
    }
  }

  public String build() {
    StringBuilder sb = new StringBuilder("@startuml\n");
    basePackage.writeClassDefinitions(sb, "");
    basePackage.writeClassRelations(sb);
    sb.append("@enduml\n");
    return sb.toString();
  }

  private class PackageContainer {

    private final NavigableMap<String, PackageContainer> subPackages = new TreeMap<>();
    private final List<ClassInfo> classes = new ArrayList<>();

    private void writeClassDefinitions(StringBuilder target, String prefix) {
      for (ClassInfo classInfo : classes) {
        writeClassInfo(classInfo, target, prefix);
      }
      for (Map.Entry<String, PackageContainer> entry : subPackages.entrySet()) {
        target.append(prefix).append("package \"").append(entry.getKey());
        PackageContainer containerToWrite = entry.getValue();
        while (containerToWrite.classes.isEmpty() && containerToWrite.subPackages.size() == 1) {
          target.append(PACKAGE_DELIMITER).append(containerToWrite.subPackages.firstKey());
          containerToWrite = containerToWrite.subPackages.firstEntry().getValue();
        }
        target.append("\" {\n");
        containerToWrite.writeClassDefinitions(target, INDENTATION + prefix);
        target.append(prefix).append("}\n");
      }
    }

    private void writeClassInfo(ClassInfo classInfo, StringBuilder target, String prefix) {
      target.append(prefix).append(getTypePrefix(classInfo)).append(" \"");
      for (ClassInfo outerClass : classInfo.getOuterClasses()) {
        target.append(outerClass.getSimpleName()).append("$");
      }
      target.append(classInfo.getSimpleName()).append("\" as ").append(classInfo.getName());
      if (!classInfo.getTypeSignatureOrTypeDescriptor().getTypeParameters().isEmpty()) {
        target.append("<");
        for (Iterator<TypeParameter> it = classInfo.getTypeSignatureOrTypeDescriptor().getTypeParameters().iterator(); it.hasNext();) {
          TypeParameter typeParameter = it.next();
          target.append(typeParameter.toStringWithSimpleNames());
          if (it.hasNext()) {
            target.append(", ");
          }
        }
        target.append(">");
      }
      target.append(" {\n");
      for (FieldInfo fieldInfo : classInfo.getDeclaredFieldInfo()) {
        if (!fieldInfo.isSynthetic() && diagram.getFieldIncludeExclude().isIncluded(fieldInfo)) {
          writeFieldInfo(fieldInfo, target, prefix);
        }
      }
      for (MethodInfo methodInfo : classInfo.getDeclaredMethodInfo()) {
        if (!methodInfo.isSynthetic() && diagram.getMethodIncludeExclude().isIncluded(methodInfo)) {
          writeMethodInfo(methodInfo, target, prefix);
        }
      }
      target.append(prefix).append("}\n");
    }

    private void writeFieldInfo(FieldInfo fieldInfo, StringBuilder target, String prefix) {
      if (fieldInfo.isEnum()) {
        target.append(prefix).append(INDENTATION).append(fieldInfo.getName()).append("\n");
        seperatorBeforeFields = true;
        seperatorBeforeMethods = true;
      } else {
        if (seperatorBeforeFields) {
          target.append(prefix).append(INDENTATION).append(DOTTED_SEPERATOR);
          seperatorBeforeFields = false;
        }

        target.append(prefix).append(INDENTATION).append(getVisibilityPrefix(fieldInfo.getModifiers()));
        appendTypeWithoutAnnotations(fieldInfo.getTypeSignatureOrTypeDescriptor(), target);
        target.append(" ").append(fieldInfo.getName()).append("\n");
        seperatorBeforeMethods = true;
      }
    }

    private void writeMethodInfo(MethodInfo methodInfo, StringBuilder target, String prefix) {
      if (seperatorBeforeMethods) {
        target.append(prefix).append(INDENTATION).append(LINE_SEPERATOR);
        seperatorBeforeMethods = false;
      }

      target.append(prefix).append(INDENTATION).append(getVisibilityPrefix(methodInfo.getModifiers()));
      appendTypeWithoutAnnotations(methodInfo.getTypeSignatureOrTypeDescriptor().getResultType(), target);
      target.append(" ").append(methodInfo.getName()).append("(");
      for (Iterator<MethodParameterInfo> it = asList(methodInfo.getParameterInfo()).iterator(); it.hasNext();) {
        MethodParameterInfo parameter = it.next();
        appendTypeWithoutAnnotations(parameter.getTypeSignatureOrTypeDescriptor(), target);
        if (it.hasNext()) {
          target.append(", ");
        }
      }
      target.append(")\n");
    }

    private void writeClassRelations(StringBuilder target) {
      for (ClassInfo classInfo : classes) {
        List<ClassInfo> excludedInterfaces = emptyList();
        if (classInfo.getSuperclass() != null) {
          excludedInterfaces = classInfo.getSuperclass().getInterfaces();
          writeClassRelation(classInfo, classInfo.getSuperclass(), new ExtensionRelation(false), target);
        }
        for (ClassInfo interfaceInfo : classInfo.getInterfaces()) {
          if (!excludedInterfaces.contains(interfaceInfo)) {
            writeClassRelation(classInfo, interfaceInfo, new ExtensionRelation(true), target);
          }
        }

        associativeRelations.getOrDefault(classInfo, emptyMap()).forEach((associatedClass, association)
                -> writeClassRelation(classInfo, associatedClass, association, target)
        );
      }

      for (PackageContainer subPackage : subPackages.values()) {
        subPackage.writeClassRelations(target);
      }
    }

    private void writeClassRelation(ClassInfo from, ClassInfo to, AbstractClassRelation relation, StringBuilder target) {
      if (!allClasses.contains(from) || !allClasses.contains(to)) {
        return;
      }

      RelationOverride override = RelationOverride.EMPTY;
      for (RelationOverride relationOverride : diagram.getRelationOverrides()) {
        if (relationOverride.matches(relation.getClass(), from, to)) {
          override = relationOverride;
          break;
        }
      }

      target.append(from.getName()).append(" ");
      override.writeArrow(relation, target);
      target.append(" ").append(to.getName());
      override.writeLabel(relation, target);
      target.append("\n");
    }

    private String getTypePrefix(ClassInfo classInfo) {
      if (classInfo.isEnum()) {
        return "enum";
      } else if (classInfo.isAnnotation()) {
        return "annotation";
      } else if (classInfo.isInterface()) {
        return "interface";
      } else if (classInfo.isAbstract()) {
        return "abstract class";
      } else if (classInfo.hasAnnotation("javax.persistence.Entity")) {
        return "entity";
      } else {
        return "class";
      }
    }

    private String getVisibilityPrefix(int modifiers) {
      String prefix;
      if (Modifier.isPrivate(modifiers)) {
        prefix = "-";
      } else if (Modifier.isProtected(modifiers)) {
        prefix = "~";
      } else if (Modifier.isPublic(modifiers)) {
        prefix = "+";
      } else {
        prefix = "#";
      }

      if (Modifier.isStatic(modifiers)) {
        prefix += " {static}";
      } else if (Modifier.isAbstract(modifiers)) {
        prefix += " {abstract}";
      }
      return prefix;
    }

    private void appendTypeWithoutAnnotations(TypeSignature typeSignature, StringBuilder sb) {
      if (typeSignature instanceof ClassRefTypeSignature) {
        ClassRefTypeSignature classRefTypeSignature = (ClassRefTypeSignature) typeSignature;
        sb.append(classRefTypeSignature.getClassInfo().getSimpleName());

        List<TypeArgument> typeArguments = classRefTypeSignature.getTypeArguments();
        // Append base class type arguments
        if (!typeArguments.isEmpty()) {
          appendTypeArguments(sb, typeArguments);
        }
      } else if (typeSignature instanceof ArrayTypeSignature) {
        ArrayTypeSignature arrayTypeSignature = (ArrayTypeSignature) typeSignature;
        appendTypeWithoutAnnotations(arrayTypeSignature.getElementTypeSignature(), sb);
        for (int i = 0; i < arrayTypeSignature.getNumDimensions(); i++) {
          sb.append("[]");
        }
      } else if (typeSignature instanceof BaseTypeSignature) {
        sb.append(((BaseTypeSignature) typeSignature).getTypeStr());
      } else if (typeSignature instanceof TypeVariableSignature) {
        sb.append(((TypeVariableSignature) typeSignature).getName());
      } else {
        throw new AssertionError("Unknown signature type " + typeSignature.getClass().getName());
      }
    }

    private void appendTypeArguments(StringBuilder sb, List<TypeArgument> typeArguments) {
      sb.append('<');
      for (Iterator<TypeArgument> it = typeArguments.iterator(); it.hasNext();) {
        sb.append(it.next().toStringWithSimpleNames());
        if (it.hasNext()) {
          sb.append(", ");
        }
      }
      sb.append('>');
    }

  }

}
