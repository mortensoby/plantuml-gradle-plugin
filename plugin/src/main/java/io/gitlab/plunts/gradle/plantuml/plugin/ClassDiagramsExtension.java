/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.plunts.gradle.plantuml.plugin;

import groovy.lang.Closure;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.FieldInfo;
import io.github.classgraph.MethodInfo;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.BaselineMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.ClassMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.FieldMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.IncludeExclude;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.InterfaceMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.MethodMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.PackageMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.ReferencedClassMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.SubclassMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.SuperclassMatcher;
import io.gitlab.plunts.gradle.plantuml.plugin.output.AbstractDiagramWriter;
import io.gitlab.plunts.gradle.plantuml.plugin.output.DefaultDiagramWriter;
import io.gitlab.plunts.gradle.plantuml.plugin.output.InsertingDiagramWriter;
import io.gitlab.plunts.gradle.plantuml.plugin.output.RenderingDiagramWriter;
import io.gitlab.plunts.gradle.plantuml.plugin.relation.AssociativeRelation;
import io.gitlab.plunts.gradle.plantuml.plugin.relation.ExtensionRelation;
import io.gitlab.plunts.gradle.plantuml.plugin.relation.RelationOverride;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.gradle.api.InvalidUserDataException;

/**
 * Gradle DSL extension to configure class diagrams for the project.
 */
@Getter
@Setter
public class ClassDiagramsExtension {

  private final List<ClassDiagram> diagrams = new ArrayList<>();
  private String plantumlServer = "https://www.plantuml.com/plantuml";

  public void diagram(Closure<ClassDiagram> closure) {
    diagram(null, closure);
  }

  public void diagram(String name, Closure<ClassDiagram> closure) {
    ClassDiagram diagram = new ClassDiagram(name);
    closure.setDelegate(diagram);
    closure.call(diagram);
    diagrams.add(diagram);
  }

  @Getter
  public static final class ClassDiagram implements Serializable {

    private String name;
    private final IncludeExclude<ClassInfo, BaselineMatcher> baselineIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<ClassInfo, SuperclassMatcher> superclassIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<ClassInfo, SubclassMatcher> subclassIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<ClassInfo, InterfaceMatcher> interfaceIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<ClassInfo, ReferencedClassMatcher> referencesIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<FieldInfo, FieldMatcher> fieldIncludeExclude = new IncludeExclude<>();
    private final IncludeExclude<MethodInfo, MethodMatcher> methodIncludeExclude = new IncludeExclude<>();
    private final Collection<RelationOverride> relationOverrides = new ArrayList<>();
    private final List<AbstractDiagramWriter> writers = new ArrayList<>();
    private boolean showPackages = true;

    public ClassDiagram(String name) {
      this.name = name;
      fieldIncludeExclude.addInclude((FieldMatcher) fields().thatArePublic());
      methodIncludeExclude.addInclude(((MethodMatcher) methods().thatArePublic()).andNotBuiltin());
    }

    public void name(String name) {
      this.name = name;
    }

    public void include(BaselineMatcher matcher) {
      baselineIncludeExclude.addInclude(matcher);
    }

    public void include(SuperclassMatcher matcher) {
      superclassIncludeExclude.addInclude(matcher);
    }

    public void include(SubclassMatcher matcher) {
      subclassIncludeExclude.addInclude(matcher);
    }

    public void include(InterfaceMatcher matcher) {
      interfaceIncludeExclude.addInclude(matcher);
    }

    public void include(ReferencedClassMatcher matcher) {
      referencesIncludeExclude.addInclude(matcher);
    }

    public void include(FieldMatcher matcher) {
      fieldIncludeExclude.addInclude(matcher);
    }

    public void include(MethodMatcher matcher) {
      methodIncludeExclude.addInclude(matcher);
    }

    public void exclude(BaselineMatcher matcher) {
      baselineIncludeExclude.addExclude(matcher);
    }

    public void exclude(SuperclassMatcher matcher) {
      superclassIncludeExclude.addExclude(matcher);
    }

    public void exclude(SubclassMatcher matcher) {
      subclassIncludeExclude.addExclude(matcher);
    }

    public void exclude(InterfaceMatcher matcher) {
      interfaceIncludeExclude.addExclude(matcher);
    }

    public void exclude(ReferencedClassMatcher matcher) {
      referencesIncludeExclude.addExclude(matcher);
    }

    public void exclude(FieldMatcher matcher) {
      fieldIncludeExclude.addExclude(matcher);
    }

    public void exclude(MethodMatcher matcher) {
      methodIncludeExclude.addExclude(matcher);
    }

    public ClassMatcher classes() {
      return new ClassMatcher();
    }

    public ClassMatcher classes(String name) {
      return new ClassMatcher().withNameLike(name);
    }

    public PackageMatcher packages() {
      return new PackageMatcher();
    }

    public PackageMatcher packages(String name) {
      return new PackageMatcher().withNameLike(name);
    }

    public SuperclassMatcher superclasses() {
      return new SuperclassMatcher();
    }

    public SubclassMatcher subclasses() {
      return new SubclassMatcher();
    }

    public InterfaceMatcher interfaces() {
      return new InterfaceMatcher();
    }

    public ReferencedClassMatcher referencedClasses() {
      return new ReferencedClassMatcher();
    }

    public FieldMatcher fields() {
      return new FieldMatcher();
    }

    public MethodMatcher methods() {
      return new MethodMatcher();
    }

    public void hidePackages() {
      showPackages = false;
    }

    public void override(RelationOverride relationOverride) {
      if (relationOverride.getFrom() == null) {
        throw new InvalidUserDataException("from not specified");
      }
      if (relationOverride.getTo() == null) {
        throw new InvalidUserDataException("from not specified");
      }
      relationOverrides.add(relationOverride);
    }

    public RelationOverride association() {
      return new RelationOverride(AssociativeRelation.class);
    }

    public RelationOverride extension() {
      return new RelationOverride(ExtensionRelation.class);
    }

    public void writeTo(File outputFile) {
      this.writers.add(new DefaultDiagramWriter(outputFile));
    }

    public void insertInto(File outputFile) {
      this.writers.add(new InsertingDiagramWriter(outputFile));
    }

    public void renderTo(File outputFile) {
      this.writers.add(new RenderingDiagramWriter(outputFile));
    }

  }

}
