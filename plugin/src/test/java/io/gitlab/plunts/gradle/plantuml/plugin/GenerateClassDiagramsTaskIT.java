/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.plunts.gradle.plantuml.plugin;

import groovy.lang.GroovyShell;
import groovyjarjarantlr.StringUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import okio.Buffer;
import org.codehaus.groovy.control.CompilationFailedException;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.gradle.api.DefaultTask;
import org.gradle.api.file.FileCollection;
import org.gradle.api.file.SourceDirectorySet;
import org.gradle.api.internal.GradleInternal;
import org.gradle.api.internal.file.temp.TemporaryFileProvider;
import org.gradle.api.internal.plugins.ExtensionContainerInternal;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.internal.project.taskfactory.TaskIdentity;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.tasks.SourceSetOutput;
import org.gradle.internal.service.ServiceRegistry;
import org.gradle.util.Path;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junitpioneer.jupiter.DisableIfTestFails;

import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@DisableIfTestFails
@TestMethodOrder(OrderAnnotation.class)
class GenerateClassDiagramsTaskIT {

  private static final Pattern PATH_SEPERATOR = Pattern.compile(",");
  private static final Set<File> OUTPUT_FILES = new HashSet<>();
  private static final Set<File> CHECKED_FILES = new HashSet<>();
  private static Buffer pngBuffer = new Buffer();
  private static Buffer svgBuffer = new Buffer();

  private static String getOutputDirectory(String dir) {
    return "build/tmp/test/" + StringUtils.stripFront(dir, '/');
  }

  @BeforeAll
  static void prepareFilesForInsertion() throws IOException {
    prepareFileForInsertion("/examples/app/README.md");
    prepareFileForInsertion("/examples/dto/README.adoc");
    Files.copy(new File("../examples/app/diagrams/vehicle_context.png").toPath(), pngBuffer.outputStream());
    Files.copy(new File("../examples/app/diagrams/vehicle_context.svg").toPath(), svgBuffer.outputStream());
  }

  private static void prepareFileForInsertion(String projectFile) throws IOException {
    File from = new File(".." + projectFile).getCanonicalFile();
    File to = new File(getOutputDirectory(projectFile)).getCanonicalFile();
    try (BufferedReader reader = Files.newBufferedReader(from.toPath())) {
      to.getParentFile().mkdirs();
      try (BufferedWriter writer = Files.newBufferedWriter(to.toPath(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
        boolean charged = false;
        boolean suppress = false;
        String line;
        while ((line = reader.readLine()) != null) {
          if (suppress && "@enduml".equals(line)) {
            suppress = false;
            charged = false;
          }
          if (!suppress) {
            writer.write(line);
            writer.write("\n");
          }
          if (line.startsWith("'")) {
            charged = true;
          } else if (charged && "@startuml".equals(line)) {
            suppress = true;
          } else {
            charged = false;
          }
        }
      }
    }
  }

  @Order(1)
  @ParameterizedTest(name = "generateClassDiagrams {0}")
  @CsvSource(delimiter = ';', value = {
    "/examples/;app/build/classes/java/main/,dto/build/classes/java/main/;FakeDependency.jar",
    "/examples/app/;build/classes/java/main/;../examples/dto/build/classes/java/main/",
    "/examples/dto/;build/classes/java/main/;FakeDependency.jar"
  })
  void generateClassDiagrams(String projectDir, String projectClasspath, String dependencyClasspath) throws Exception {
    MockWebServer server = new MockWebServer();
    server.setDispatcher(new Dispatcher() {
      @Override
      public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
        if (request.getPath().contains("png")) {
          return new MockResponse().setBody(pngBuffer);
        } else if (request.getPath().contains("svg")) {
          return new MockResponse().setBody(svgBuffer);
        } else {
          return new MockResponse().setHttp2ErrorCode(404).setStatus("Not Found");
        }
      }
    });
    server.start();

    try {
      ClassDiagramsExtension extension = parseBuildScript(projectDir);
      extension.setPlantumlServer("http://" + server.getHostName() + ":" + server.getPort());
      ProjectInternal project = mockProject(projectDir, projectClasspath, dependencyClasspath);
      TaskIdentity<GenerateClassDiagramsTask> identity = TaskIdentity.create("generateClassDiagrams", GenerateClassDiagramsTask.class, project);
      GenerateClassDiagramsTask task = DefaultTask.injectIntoNewInstance(project, identity, () -> new GenerateClassDiagramsTask(extension));
      task.addProjectClasspath(project);
      task.generateClassDiagrams();
      assertThat(task.getOutputFiles(), is(not(empty())));
      OUTPUT_FILES.addAll(task.getOutputFiles());
    } finally {
      server.shutdown();
    }
  }

  @Order(2)
  @ParameterizedTest(name = "compareOutput {0}")
  @CsvSource({
    "/examples/diagrams/full_package.puml",
    "/examples/app/README.md",
    "/examples/app/diagrams/app_only.puml",
    "/examples/app/diagrams/classs_with_c.puml",
    "/examples/app/diagrams/controllers_with_references.puml",
    "/examples/app/diagrams/entities.puml",
    "/examples/app/diagrams/filtered_members.puml",
    "/examples/app/diagrams/full_package.puml",
    "/examples/app/diagrams/private_members.puml",
    "/examples/app/diagrams/relation_override.puml",
    "/examples/app/diagrams/vehicle_context.png",
    "/examples/app/diagrams/vehicle_context.puml",
    "/examples/app/diagrams/vehicle_context.svg",
    "/examples/dto/README.adoc",
    "/examples/dto/diagrams/full_package.puml"
  })
  void compareOutput(String baseline) throws Exception {
    final File baselineFile = new File(".." + baseline).getCanonicalFile();
    final File testOutputFile = new File(getOutputDirectory(baseline)).getCanonicalFile();
    CHECKED_FILES.add(testOutputFile);

    if (baseline.endsWith("puml")) {
      List<String> expectedLines = Files.readAllLines(baselineFile.toPath());
      List<String> writtenLines = Files.readAllLines(testOutputFile.toPath());
      assertThat(writtenLines, contains(expectedLines.toArray(new String[expectedLines.size()])));
    } else {
      final byte[] expectedBytes = Files.readAllBytes(baselineFile.toPath());
      final byte[] writtenBytes = Files.readAllBytes(testOutputFile.toPath());
      assertTrue(Arrays.equals(expectedBytes, writtenBytes));
    }
  }

  @Order(3)
  @Test
  void wasEveryOutputChecked() throws Exception {
    assertThat(CHECKED_FILES, containsInAnyOrder(OUTPUT_FILES.toArray(new File[OUTPUT_FILES.size()])));
  }

  private ClassDiagramsExtension parseBuildScript(String projectDir) throws IOException, CompilationFailedException {
    CompilerConfiguration config = new CompilerConfiguration();
    config.setScriptBaseClass(GradleScriptMock.class.getName());
    GroovyShell shell = new GroovyShell(config);
    GradleScriptMock script = (GradleScriptMock) shell.parse(new File(".." + projectDir + "build.gradle"));
    script.setBaseDir(getOutputDirectory(projectDir));
    script.run();
    return script.getExtension();
  }

  private ProjectInternal mockProject(String projectDir, String projectClasspath, String dependencyClasspath) {
    ProjectInternal project = mock(ProjectInternal.class);
    when(project.projectPath(any(String.class))).thenAnswer(invoc -> Path.path(":" + invoc.getArgumentAt(0, String.class)));
    when(project.identityPath(any(String.class))).thenAnswer(invoc -> Path.path(":" + invoc.getArgumentAt(0, String.class)));

    GradleInternal gradle = mock(GradleInternal.class);
    when(project.getGradle()).thenReturn(gradle);
    when(gradle.getIdentityPath()).thenReturn(Path.path(":"));

    ServiceRegistry serviceRegistry = mock(ServiceRegistry.class);
    when(project.getServices()).thenReturn(serviceRegistry);

    TemporaryFileProvider temporaryFileProvider = mock(TemporaryFileProvider.class);
    when(temporaryFileProvider.newTemporaryDirectory(any(String.class)))
            .thenAnswer(invoc -> {
              final File tmpDir = new File(getOutputDirectory(invoc.getArgumentAt(0, String.class)));
              tmpDir.mkdirs();
              return tmpDir;
            });
    when(serviceRegistry.get(TemporaryFileProvider.class)).thenReturn(temporaryFileProvider);

    ObjectFactory objectFactory = mock(ObjectFactory.class);
    when(project.getObjects()).thenReturn(objectFactory);

    ExtensionContainerInternal extensionContainer = mock(ExtensionContainerInternal.class);
    when(project.getExtensions()).thenReturn(extensionContainer);

    JavaPluginExtension javaPluginExtension = mock(JavaPluginExtension.class);
    when(extensionContainer.findByType(JavaPluginExtension.class)).thenReturn(javaPluginExtension);

    SourceSetContainer sourceSets = mock(SourceSetContainer.class);
    when(javaPluginExtension.getSourceSets()).thenReturn(sourceSets);
    when(extensionContainer.findByType(SourceSetContainer.class)).thenReturn(sourceSets);

    SourceSet sourceSet = mock(SourceSet.class);
    when(sourceSets.getByName("main")).thenReturn(sourceSet);

    SourceSetOutput sourceSetOutput = mock(SourceSetOutput.class);
    when(sourceSet.getOutput()).thenReturn(sourceSetOutput);

    FileCollection classesDirs = mock(FileCollection.class);
    when(sourceSetOutput.getClassesDirs()).thenReturn(classesDirs);
    when(classesDirs.getFiles()).thenReturn(PATH_SEPERATOR.splitAsStream(projectClasspath)
            .map(s -> new File(".." + projectDir + s))
            .map(GenerateClassDiagramsTaskIT::toCanonicalFile)
            .collect(toSet()));

    FileCollection compileClasspath = mock(FileCollection.class);
    when(sourceSet.getCompileClasspath()).thenReturn(compileClasspath);
    when(compileClasspath.getFiles()).thenReturn(PATH_SEPERATOR.splitAsStream(dependencyClasspath)
            .map(s -> new File(dependencyClasspath))
            .map(GenerateClassDiagramsTaskIT::toCanonicalFile)
            .collect(toSet()));

    SourceDirectorySet allSources = mock(SourceDirectorySet.class);
    when(sourceSet.getAllSource()).thenReturn(allSources);
    when(allSources.getFiles()).thenReturn(emptySet());

    return project;
  }

  private static File toCanonicalFile(File relative) {
    try {
      return relative.getCanonicalFile();
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }

}
