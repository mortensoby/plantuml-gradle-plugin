package io.gitlab.plunts.gradle.plantuml.plugin.matcher;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ScanResult;
import io.gitlab.plunts.gradle.plantuml.plugin.matcher.subjects.ClassWithMethodNamedGetSetAndIs;
import org.junit.jupiter.api.Test;

class MethodMatcherTest {

    @Test
    void testWithMethodNamesGetSetAndIs() {
        ClassInfo classInfo =
            new ClassGraph().enableAllInfo().acceptPackages("io.gitlab.plunts.gradle.plantuml.plugin.matcher.subjects").scan().getClassInfo(ClassWithMethodNamedGetSetAndIs.class.getName());
        MethodMatcher matcher = new MethodMatcher();

        assert matcher.test(classInfo.getMethodInfo("get").get(0));
        assert matcher.test(classInfo.getMethodInfo("set").get(0));
        assert matcher.test(classInfo.getMethodInfo("is").get(0));
    }
}