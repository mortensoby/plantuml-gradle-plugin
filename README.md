# plantuml-gradle-plugin

Gradle plugin to build PlantUML diagrams from code (for living and up-to-date documentation).

## Activation

Add the plugin to the `build.gradle` of your project:

```groovy
plugins {
  id "io.gitlab.plunts.plantuml" version "2.0.2"
}
```

## Configuration

After the plugin was added to your project, you can configure as many diagrams as you like using the following extension:

```groovy
classDiagrams {
  diagram {
    name "Visualise Package"
    include packages().withName("x.y.z")
    writeTo file('xyz.puml')
  }
}
```

The above example writes a single diagram of a full package to a file.
This can be highly customized to create as many views on your code as necessary.
The following sections describe the full range of keywords that can be used.
For a "learning by example" experience, see the [example project](examples/app/build.gradle).

### Include / Exclude

Each diagram **must** include at least one package or class, which is done using the `include` keyword followed by either `classes()` or `packages()`.
The name of the package/class is either specified exactly using `.withName("x.y.z")` or via glob using `.withNameLike("x.y.*")`.
Conversely the `exclude` keyword can be used to remove unwanted elements from the diagram.

Starting with the included classes, other elements can be included (or excluded) as well:

* `superclasses()` includes all superclasses of already included classes
* `subclasses()` includes all subclasses of already included classes
* `interfaces()` includes all implemented interfaces of already included classes
* `referencedClasses()` includes all classes that are used in fields of already included classes
* `fields()` is used to control which fields are shown (default: public)
* `methods()` is used to control which fields are shown (default: public)

Each element can be further tailored using the following methods:

* `insideOfProject()` and `outsideOfProject()`
  * Match elements that are part of your project.
    For fields and methods this refers to the declaring class.
    If the project has subprojects, they are considered to be *inside* of the parent project.
  * Applicable to: packages, classes, fields and methods
* `withName(String)` and `withNameLike(String)`
  * Match elements by their name.
    For classes, the full name is tested.
  * Applicable to: packages, classes, fields and methods
* `nonRecursive()`
  * *Sub-Packages* are matched by default, meaning `packages().withName("x.y")` also matches the package "x.y.z".
    Use this configuration to stop this behaivour.
* `withParentClass()` and `withoutParentClass()`
  * Configure whether or not to include inner *classes* (i.e. parents with parent classes).
* `annotatedWith(String)`
  * Match elements that are annotated with the given annotation (full qualified).
  * Applicable to: classes, fields and methods
* `thatAreXY()`, `andAreXY()` and `orAreXY()`
  * Match elements having the given modifiers.
    Must be started with `thatAre` and can then be chained with `andAre`.
    Visibility modifiers can also be combined using `orAre`.
    To inverse the meaning, use the `not` keyword, e.g. `thatAreNotStatic()`.
  * Supported modifiers: `public`, `protected`, `packagePrivate`, `private`, `static`, `abstract`, `final`, `volatile`, `transient`
  and `native`.
  * Applicable to: classes, fields and methods
* `thatAreAccessors()` and `thatAreNotAccessors()`
  * Match *methods* that are accessors (getter/setter) for a field.
* `thatHaveAccessors()` and `thatDontHaveAccessors()`
  * Match *fields* that have at least one accessor method (getter/setter).
* `of(String)`, `of(classes())`, `of(packages())`
  * For fields/methods: match members inside the specified class/package.
  * For superclasses/subclasses/interfaces/referencedClasses: match classes that are referenced by the given class/package.
* `inPackage(String)`
  * Finding other classes isn't easy, so you may have to use this additional call to hint where they may be.
  * Applicable to: superclasses, subclasses, interfaces, referencedClasses

### Override Relations

The plugin tries it's best to use the correct arrows to show relations between classes.
This explicitly includes support for annotations used by the Java Persistence and Validation API.
If the result is not what you want to see, you can use the `override` keyword to change it.
Two kinds of relations can be overridden:

* `association()` is a relation determined by the fields of the class.
* `extension()` is the relation to superclasses and interfaces.

In both cases, the following methods are used to configure the override:

* `from(String)` and `to(String)` are used to define the classes of the relationship that shall be changed.
  Both accept glob patterns.
* `with(String)` overrides the whole relation. You may want to use it for heavy customizing needs.
* `withSourceMultiplicity(String)` and `withTargetMultiplicity(String)` override the multiplicity (e.g. "0..*").
* `withSourceArrow(String)` and `withTargetArrow(String)` changes the arrow only (e.g. "<" or ">").
* `withLineCharacter(String)` changes the line character (e.g. "-" or ".").
* `withStyle(String)` changes the style of the arrow (e.g. "down").
* `withLabel(String)` changes the label of the relation (never set automatically).

### Styling

The only available global style option is to hide the packages using `hidePackages()`.
Future versions may add more possibilities here.

### Output

Last but not least you have to configure where to store the diagram.
The `writeTo` command accepts any `file` and will overwrite it's content.
Alternativly, you can use `insertInto` to inject the diagram into another file, e.g. Markdown or AsciiDoc.
Use the diagram name to mark the location with a PlantUML comment:

```
'DiagramName
@startuml
@enduml
```

If the marker can't be found, the task will generate an error message with this exact instructions.
The [last diagram](examples/app/build.gradle) of the [example project](examples/app/README.md) shows how it is done in practice.

As a third option, you can directly render the diagram using a PlantUML server of your choice (defaults to the public one).
To do this, use the `renderTo` command and specify a png or svg file.

## Usage

Run the task `generateClassDiagrams` using gradle:

```
./gradlew generateClassDiagrams
```

## Credits

This project started as a fork of a plugin by [RoRoche](https://github.com/RoRoche/plantuml-gradle-plugin), which is unmaintained for a while now.
It ended up being a complete rewrite, but takes a lot of inspiration from it.
As the original, this is still build on top of [ClassGraph](https://github.com/classgraph/classgraph).
