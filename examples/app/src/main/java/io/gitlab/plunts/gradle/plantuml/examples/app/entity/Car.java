package io.gitlab.plunts.gradle.plantuml.examples.app.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Car extends Vehicle {

  private int maxPassengers;

}
