package io.gitlab.plunts.gradle.plantuml.examples.app.entity;

import java.time.Instant;
import javax.persistence.Embeddable;
import lombok.Data;

@Data
@Embeddable
public class Trip {

  private Instant startTime;
  private Instant endTime;

}
